{-# LANGUAGE DeriveGeneric #-}

module Common.Message (C2S(..), S2C(..), Lettering(..)) where

import Data.Aeson (ToJSON, FromJSON, toEncoding, parseJSON,
  defaultOptions, genericToEncoding, genericParseJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import Numeric.Natural (Natural)

data Lettering = Lettering {
  _info :: Text,
  _leftPlayer :: Text,
  _rightPlayer :: Text} deriving (Eq, Show, Generic)

instance ToJSON Lettering where toEncoding = genericToEncoding defaultOptions
instance FromJSON Lettering where parseJSON = genericParseJSON defaultOptions

data C2S =
  C2SWrite FilePath Text |
  C2SLoad FilePath |
  C2SMatchSheets String Natural [Lettering] deriving (Eq, Show, Generic)

instance ToJSON C2S where toEncoding = genericToEncoding defaultOptions
instance FromJSON C2S where parseJSON = genericParseJSON defaultOptions

newtype S2C = S2CLoad {fileContent :: String} deriving (Eq, Show, Generic)

instance ToJSON S2C where toEncoding = genericToEncoding defaultOptions
instance FromJSON S2C where parseJSON = genericParseJSON defaultOptions
