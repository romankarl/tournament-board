import Control.Monad ((>=>))
import Data.Bifunctor (bimap)
import Data.Function (on)
import Data.List (transpose, unfoldr)

rankingFile :: FilePath
rankingFile = "swiss_Einzel_ranking.txt"

outs :: [FilePath]
outs = (\a -> concat ["group_", a, ".txt"]) <$> ["Hoch", "Mittel", "Niedrig"]

maxGroupSize :: Int
maxGroupSize = 4

playerList :: IO [String]
playerList = lines <$> readFile rankingFile

main :: IO ()
main = do
 a <- playerList
 let (f,(g,h)) = splitAt 30 <$> splitAt 30 a
 mapM_ (uncurry writeFile) $ zipWith (\out -> (,) out . showTournament) outs [f, g, h]

showTournament :: [String] -> String
showTournament = ("setsToWin 3\n\n" <>) . showGroups

showGroups :: [String] -> String
showGroups = formGroups >=> (<> "\n\n") . unlines

formGroups :: [a] -> [[a]]
formGroups as = snakeTranspose $
  snakeSplit (ceiling $ on (/) fromIntegral (length as) maxGroupSize) as

snakeSplit :: Int -> [a] -> [[a]]
snakeSplit numGroups a = unfoldr (coalg numGroups) (a, False)

coalg :: Int -> ([a], Bool) -> Maybe ([a], ([a], Bool))
coalg _ ([], _) = Nothing
coalg numGroups (b, r) = Just . bimap (if r then reverse else id) (flip (,) $ not r) $
  splitAt numGroups b

snakeTranspose :: [[a]] -> [[a]]
snakeTranspose as | odd (length as) = transpose as
snakeTranspose as = reverse . transpose $ reverse <$> as
