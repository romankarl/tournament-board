{-# LANGUAGE OverloadedStrings #-}

module Group.GroupTypes where

import Data.Text (Text)
import qualified Data.Text as T

import CommonTypes

import Group.CompleteCompetition

data GroupTournament = GroupTournament {
  tournamentParameters :: TournamentParameters,
  tournamentGroups :: [Group]
}

data Group = Group { players :: [Text], results :: CompleteCompetition }

showGroup :: Group -> Text
showGroup (Group ps rs) = T.intercalate "\n\n" [T.intercalate "\n" ps, showCompleteCompetition rs]

showTournament :: GroupTournament -> Text
showTournament (GroupTournament params gs) =
  T.intercalate "\n\n" [showTournamentParameters params, T.intercalate "\n" $ showGroup <$> gs]
