{-# LANGUAGE OverloadedStrings #-}

module Group.CompleteCompetition where

import Control.Applicative (liftA2)
import Data.Maybe (catMaybes)
import Data.Foldable (fold)
import Data.Function (on)
import Data.List (foldl', transpose, sort, sortOn)
import qualified Data.Map as Map
import Data.Ord (Down(Down))
import Data.Text (Text)
import qualified Data.Text as T
import Numeric.Natural (Natural)

import CommonTypes

newtype CompleteCompetition = CompleteCompetition [[Maybe MatchResult]]
data PlayerBalance a = PlayerBalance {playerId :: a, stats :: Stats, directStats :: Maybe Stats}
data Stats = Stats {wins :: Natural, losses :: Natural, wonSets :: Natural, lostSets :: Natural}
  deriving Eq

showCompleteCompetition :: CompleteCompetition -> Text
showCompleteCompetition (CompleteCompetition mss) = T.concat . fmap (<> "\n") $ T.unwords . fmap (maybe "-" showMatchResult) <$> mss

instance Eq (PlayerBalance a) where
  (PlayerBalance _ a a') == (PlayerBalance _ b b') = a == b && a' == b'

instance Ord (PlayerBalance a) where
  compare (PlayerBalance _ a a') (PlayerBalance _ b b') =
    fold [mcomp a b, compMaybe mcomp a' b', compMaybe scomp a' b', scomp a b]
      where
        mcomp x y = compare (wins x) (wins y)
        scomp (Stats _ _ 0 0) (Stats _ _ 0 0) = EQ
        scomp (Stats _ _ w l) (Stats _ _ 0 0) | w >= l = GT
        scomp _ (Stats _ _ 0 0) = LT
        scomp x y = compare (setRelation x :: Double) (setRelation y :: Double)
        setRelation s = on (/) fromIntegral (wonSets s) (lostSets s)
        compMaybe f x y = fold $ liftA2 f x y

emptyCompetition :: Int -> CompleteCompetition
emptyCompetition n = CompleteCompetition $ (`replicate` Nothing) <$> [n-1, n-2 .. 1]

competitionRanking :: Natural -> [a] -> CompleteCompetition -> [PlayerBalance a]
competitionRanking wsets ps c =
  sortOn Down . (zipWith3 PlayerBalance ps <*> directResults wsets c) $ statsFromCompetition wsets c

directResults :: Natural -> CompleteCompetition -> [Stats] -> [Maybe Stats]
directResults wsets c ss =
  let
    lenNat = fromInteger . toInteger . length
    dStats =
      equalWins ((\s -> if wins s + losses s == lenNat ss - 1 then Just s else Nothing) <$> ss) >>=
      (zip <*> statsFromCompetition wsets . subCompetition c)
  in (`lookup` dStats) <$> zipWith const [0..] ss

equalWins :: [Maybe Stats] -> [[Int]]
equalWins ss =
  fmap sort . Map.elems . Map.filter ((>1) . length) . Map.fromListWith (++) . catMaybes $
  zipWith (\ s b -> flip (,) [b] . wins <$> s) ss [0..]

statsFromCompetition :: Natural -> CompleteCompetition -> [Stats]
statsFromCompetition wsets = fmap (playerStats wsets) . playerMatchLists

playerMatchLists :: CompleteCompetition -> [[Maybe MatchResult]]
playerMatchLists c@(CompleteCompetition mss) = zipWith (++) (columns c) (mss ++ [[]])

subCompetition :: CompleteCompetition -> [Int] -> CompleteCompetition
subCompetition c = CompleteCompetition . subCompetition' c

subCompetition' :: CompleteCompetition -> [Int] -> [[Maybe MatchResult]]
subCompetition' _ [] = []
subCompetition' _ [_] = []
subCompetition' c (i:is) = takeRow i is c : subCompetition' c is

takeRow :: Int -> [Int] -> CompleteCompetition -> [Maybe MatchResult]
takeRow i is (CompleteCompetition mss) = (mss !! i !!) . (-1-i+) <$> is

playerStats :: Natural -> [Maybe MatchResult] -> Stats
playerStats wsets = foldl' (registerMatch wsets) (Stats 0 0 0 0)

registerMatch :: Natural -> Stats -> Maybe MatchResult -> Stats
registerMatch _ b Nothing = b
registerMatch wsets (Stats a b c d) (Just (MatchResult LeftPlayer osets)) =
  Stats (a+1) b (c + wsets) (d + osets)
registerMatch wsets (Stats a b c d) (Just (MatchResult RightPlayer osets)) =
  Stats a (b+1) (c + osets) (d + wsets)

columns :: CompleteCompetition -> [[Maybe MatchResult]]
columns (CompleteCompetition mss) = ([]:) . reverse . transpose $
  reverse . fmap (fmap inverseMatch) <$> mss

inverseMatch :: MatchResult -> MatchResult
inverseMatch (MatchResult LeftPlayer osets) = MatchResult RightPlayer osets
inverseMatch (MatchResult RightPlayer osets) = MatchResult LeftPlayer osets
