{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE FlexibleContexts #-}

module Widget.SwissSystemTournament where

import Control.Applicative (liftA3)
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Function (on)
import Data.List ((\\), intersperse, sortOn)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Ord (Down(Down), getDown)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import Numeric.Natural (Natural)

import Reflex.Dom.Core

import CommonTypes
import Common.Message (C2S(..), Lettering(..), S2C(..))
import Parser.SwissSystemTournamentParser (parseTournament)
import Swiss.Pairing (
  Pairing(Against, Bye), Solution(Solution), UnorderedPair(UnorderedPair), solution)
import Swiss.Points (pointsAfterMatch)
import Swiss.SwissTypes
import Widget.MessageChannel (sendAndReceiveMessages)

data PlayerStateEvent = NameChange Text | Activity
data MatchStateEvent = WinnerToggle Player | SetChange MatchResult

initialParticipants :: [Participant]
initialParticipants = map (toParticipant 0) [1..9]

tournamentFile :: Text -> FilePath
tournamentFile a = T.unpack $ T.concat ["swiss_", a ,".txt"]

rankingFile :: Text -> FilePath
rankingFile a = T.unpack $ T.concat ["swiss_", a ,"_ranking.txt"]

swissSystemTournament :: (
  DomBuilder t m,
  PostBuild t m,
  MonadHold t m,
  Prerender t m,
  PerformEvent t m,
  MonadIO (Performable m),
  MonadFix m) => Maybe Text -> Text -> m ()
swissSystemTournament baseRoute tName = do
  rec
    let
      tFile = tournamentFile tName
      initParams = TournamentParameters 3
    params <- holdDyn initParams (tournamentParameters <$> load)
    bSave <- button "Speichern"
    let
      save = tag
        (current $ zipDynWith (\ps (as,bs) -> SwissSystemTournament ps as bs) params tournamentData)
        bSave
    _ <- sendAndReceiveMessages baseRoute $ C2SWrite tFile . showTournament <$> save
    let
      saveR = tag
        (current $
          zipDynWith (\a b -> sortDrop $ zip (Down <$> a) b) ppoints (fst <$> tournamentData))
        bSave
    _ <- sendAndReceiveMessages baseRoute $ C2SWrite (rankingFile tName) . showNames <$> saveR
    msgLoad <- button "Laden" >>= sendAndReceiveMessages baseRoute . (C2SLoad tFile <$)
    let (loadError, load) = fanEither $ parseTournament . fileContent <$> msgLoad
    performEvent_ $ liftIO . print <$> loadError
    (tournamentData, ppoints) <- ((>>= fst) &&& (>>= snd)) <$>
      widgetHold
        (tournamentWidget baseRoute $ SwissSystemTournament initParams initialParticipants [])
        (tournamentWidget baseRoute <$> load)
  return ()

tournamentWidget :: (DomBuilder t m, PostBuild t m, MonadHold t m, Prerender t m, MonadFix m) =>
  Maybe Text -> SwissSystemTournament -> m (Dynamic t ([Participant], [Round]), Dynamic t [Int])
tournamentWidget baseRoute (SwissSystemTournament params initParticipants initRounds) = do
  rec
    editMode <- button "Bearbeitungsmodus" >>= toggle False
    participants <- playerBoard editMode (length . fst <$> rounds) (snd <$> rounds) initParticipants
    rounds <- tournamentBoard baseRoute params editMode participants initRounds
  pure (zipDyn participants (fst <$> rounds), snd <$> rounds)

toParticipant :: Int -> Int -> Participant
toParticipant r n = Participant (T.concat ["Spieler ", T.pack $ show n]) 1000 False r

playerBoard :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) =>
  Dynamic t Bool -> Dynamic t Int -> Dynamic t [Int] -> [Participant] -> m (Dynamic t [Participant])
playerBoard editMode roundNr ppoints initParticipants =
  divClass "plist" $ do
    rec
      let
        psE = attachWith (const . playerList editMode ppoints)
          (current $ zipDynWith (\xs r -> xs ++ [toParticipant r (length xs + 1)]) ps roundNr)
          newPlayer
      ps <- join <$> widgetHold (playerList editMode ppoints initParticipants) psE
      newPlayer <- editModeButton editMode "Spieler hinzufügen"
    pure ps

editModeButton :: (DomBuilder t m, PostBuild t m, MonadHold t m) =>
  Dynamic t Bool -> Text -> m (Event t ())
editModeButton editMode label = editableComponentWithEvent editMode (pure ()) (button label)

editableComponentWithEvent :: (DomBuilder t m, PostBuild t m, MonadHold t m) =>
  Dynamic t Bool -> m a -> m (Event t b) -> m (Event t b)
editableComponentWithEvent editMode cFix cEdit =
  editableComponent editMode (cFix >> pure never) cEdit >>= switchHold never

editableComponent :: (Adjustable t m, NotReady t m, PostBuild t m) =>
  Dynamic t Bool -> m a -> m a -> m (Event t a)
editableComponent editMode cFix cEdit = dyn $ (\ e -> if e then cEdit else cFix) <$> editMode

playerList :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) =>
  Dynamic t Bool -> Dynamic t [Int] -> [Participant] -> m (Dynamic t [Participant])
playerList editMode ppoints participants = do
  rec
    combinedDyn <- zipDynWithChangeOnFst (zipWith $ (,) . Down) ppoints playersD
    ps <- dyn $ fmap sequenceA . traverseOrdered getDown (participant editMode) <$> combinedDyn
    playersD <- join <$> holdDyn (pure participants) ps
  pure playersD

zipDynWithChangeOnFst :: (Reflex t, Eq a, MonadHold t m, MonadFix m) =>
  (a -> b -> c) -> Dynamic t a -> Dynamic t b -> m (Dynamic t c)
zipDynWithChangeOnFst f d1 d2 = fmap (uncurry f) <$> holdUniqDynBy ((==) `on` fst) (zipDyn d1 d2)

traverseOrdered :: (Applicative f, Ord c) => (c -> d) -> (d -> a -> f b) -> [(c,a)] -> f [b]
traverseOrdered h f = fmap sortDrop . traverse g . sortOn fst . zipWith reorder ([0..] :: [Int])
  where
    reorder i (c,a) = (c, (i,a))
    g (c, (i,a)) = (,) i <$> f (h c) a

sortDrop :: Ord b => [(b,a)] -> [a]
sortDrop = fmap snd . sortOn fst

participant :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) =>
  Dynamic t Bool -> Int -> Participant -> m (Dynamic t Participant)
participant editMode ppoints p = el "div" $ do
  rec
    partD' <- zipDynWithChangeOnFst (\ _ x -> x) editMode partD
    nameE <- divClass "pcolumn1" $
      editableComponentWithEvent editMode
        (dynText $ name <$> partD')
        (componentOnDyn nameField partD')
    divClass "pcolumn2" . text . T.pack $ show ppoints
    pause <- divClass "pcolumn3" $
      editableComponentWithEvent editMode
        (dyn_ $ (\x -> when (inactive x) $ text " pausierend") <$> partD')
        (pauseButton partD)
    partD <- accumDyn collectPlayerEvents p $
      leftmost [NameChange <$> nameE, Activity <$ pause]
  return partD

collectPlayerEvents :: Participant -> PlayerStateEvent -> Participant
collectPlayerEvents p (NameChange n) = p {name = n}
collectPlayerEvents p Activity = p {inactive = not $ inactive p}

componentOnDyn :: (DomBuilder t m, PostBuild t m, MonadHold t m) =>
  (a -> m (Event t b)) -> Dynamic t a -> m (Event t b)
componentOnDyn f a = dyn (f <$> a) >>= switchHold never

nameField :: DomBuilder t m => Participant -> m (Event t Text)
nameField p = do
  nameIn <- inputElement $ def & inputElementConfig_initialValue .~ name p
  pure $ _inputElement_input nameIn

pauseButton :: (PostBuild t m, DomBuilder t m) => Dynamic t Participant -> m (Event t ())
pauseButton p = do
  (e, _) <- elDynAttr' "button" def .
    dynText $ pauseText . inactive <$> p
  return $ domEvent Click e
  where
    pauseText False = "pausieren"
    pauseText True = "Pause beenden"

tournamentBoard :: (
    DomBuilder t m,
    PostBuild t m,
    MonadHold t m,
    Prerender t m,
    MonadFix m
  ) =>
  Maybe Text ->
  TournamentParameters ->
  Dynamic t Bool ->
  Dynamic t [Participant] ->
  [Round] -> m (Dynamic t ([Round], [Int]))
tournamentBoard baseRoute params editMode players initRounds = do
  rec
    nextRound <- button "nächste Runde"
    dyn_ $ uncurry (matchSheetButton baseRoute params players) <$> rounds
    rounds <- divClass "rounds" $ do
      rec
        let
          ppoints = fmap points . filter ((<=1) . entryRound) <$> players
          roundE = attachWith
            (const . tournamentTable params editMode players ppoints 0)
            (current $ zipDynWith startNextRound rounds players)
            nextRound
        rounds <- join <$>
          widgetHold (tournamentTable params editMode players ppoints 0 initRounds) roundE
      pure rounds
  pure rounds

matchSheetButton :: (DomBuilder t m, Prerender t m) =>
  Maybe Text -> TournamentParameters -> Dynamic t [Participant] -> [Round] -> [Int] -> m ()
matchSheetButton baseRoute (TournamentParameters stw) players rs ppoints = unless (null rs) $ do
  createSheets <- tag (current players) <$> button "Match-Zettel erstellen"
  let
    rl = roundLabel $ length rs
    sendEv = C2SMatchSheets (T.unpack rl) stw .
      createLettering' (last rs) ppoints rl <$>
      createSheets
  _ <- sendAndReceiveMessages baseRoute sendEv
  pure ()

createLettering :: Round -> [Int] -> Text -> [Participant] -> [Lettering]
createLettering (Round ms _ _) ppoints info players =
  (\ m -> Lettering info (textForPlayer $ player1 m) (textForPlayer $ player2 m)) <$> ms
    where textForPlayer p = showWithPoints (players !! p) (ppoints !! p)

-- code duplication in order to evade a GHCJS compiler error
createLettering' :: Round -> [Int] -> Text -> [Participant] -> [Lettering]
createLettering' (Round ms _ _) ppoints info players =
  (\ m -> Lettering info (textForPlayer $ player1 m) (textForPlayer $ player2 m)) <$> ms
    where textForPlayer p = T.concat [name (players !! p), " (", T.pack $ show (ppoints !! p), ")"]

roundLabel :: Int -> Text
roundLabel n = "Runde " <> T.pack (show n)

tournamentTable :: (PostBuild t m, DomBuilder t m, MonadHold t m, MonadFix m) =>
  TournamentParameters ->
  Dynamic t Bool ->
  Dynamic t [Participant] ->
  Dynamic t [Int] ->
  Int ->
  [Round] -> m (Dynamic t ([Round], [Int]))
tournamentTable _ _ _ ppoints _ [] = return $ (,) [] <$> ppoints
tournamentTable params editMode players ppoints roundNr (a:as) = do
  (tRound, ppoints') <- pairingForm
    params
    (if null as then constDyn True else editMode)
    players
    ppoints
    roundNr
    a
  let
    pointsForNextRound = zipDynWith (++) (sequenceA ppoints') $
      fmap points . filter ((\x -> x == roundNr+1 || x == roundNr+2) . entryRound) <$> players
  dynerg <- tournamentTable params editMode players pointsForNextRound (roundNr+1) as
  pure $ zipDynWith (\ m (ms,b) -> (m : ms, b)) tRound dynerg

startNextRound :: ([Round], [Int]) -> [Participant] -> [Round]
startNextRound (rounds, ppoints) players =
  let
    inact = fst <$> filter (inactive . snd) (zip [0..] players)
    lateEntrants = fst <$> filter ((/=0) . entryRound . snd) (zip [0..] players)
    constraints = Set.fromList $ (rounds >>= fromRound) ++ (Bye <$> lateEntrants)
    s = toRound . fromMaybe (Solution [] Nothing) . solution constraints .
      (\\ inact) . fmap fst . sortOn (negate . snd) $ zip [0..] ppoints
    fromRound (Round ms rBye is) = fmap fromMatch ms ++ (Bye <$> foldr (:) is rBye)
    fromMatch (Match a b _) = Against a b
    toRound (Solution ps b) = Round (toMatch <$> ps) b inact
    toMatch (UnorderedPair a b) = Match a b Nothing
  in rounds ++ [s]

pairingForm :: (PostBuild t m, DomBuilder t m, MonadHold t m, MonadFix m) =>
  TournamentParameters ->
  Dynamic t Bool ->
  Dynamic t [Participant] ->
  Dynamic t [Int] ->
  Int ->
  Round -> m (Dynamic t Round, [Dynamic t Int])
pairingForm params editMode players ppoints roundNr (Round ms rBye inact) = divClass "pairing" $ do
  el "h3" . text . roundLabel $ roundNr+1
  let ptsOf p = (!! p) <$> ppoints
  as <- traverse (\x -> match params editMode players x (ptsOf $ player1 x) (ptsOf $ player2 x)) ms
  let
    resultingPoints =
      orderPoints ((\b -> (b, ptsOf b)) <$> foldr (:) inact rBye) . zip ms $ snd <$> as
  divClass "further" $ do
    mapM_ (\ n -> dyn_ $ (\ ps -> listOtherParticipants "Freilos: " [ps!!n]) <$> players) rBye
    dyn_ $ listOtherParticipants "pausierend: " . (\p -> (p!!) <$> inact) <$> players
    let entrants = drop (length resultingPoints) <$> zipDynWith (zipWith const) players ppoints
    dyn_ $ listOtherParticipants "Einstieg: " <$> entrants
  return ((\xs -> Round xs rBye inact) <$> traverse fst as, resultingPoints)

listOtherParticipants :: (PostBuild t m, DomBuilder t m) => Text -> [Participant] -> m ()
listOtherParticipants label ps = unless (null ps) . el "div" $ do
  divClass "fcolumn1" $ text label
  divClass "fcolumn2" . sequence_ . intersperse (el "br" blank) $ text . name <$> ps

orderPoints ::
  [(ParticipantId, Dynamic t Int)] ->
  [(Match, (Dynamic t Int, Dynamic t Int))] -> [Dynamic t Int]
orderPoints b as = sortDrop $ (as >>= applyMatch) ++ b
  where
    applyMatch (Match p1 p2 _, (d1, d2)) = [(p1, d1), (p2, d2)]

match :: (PostBuild t m, DomBuilder t m, MonadHold t m, MonadFix m) =>
  TournamentParameters ->
  Dynamic t Bool ->
  Dynamic t [Participant] ->
  Match ->
  Dynamic t Int ->
  Dynamic t Int -> m (Dynamic t Match, (Dynamic t Int, Dynamic t Int))
match (TournamentParameters stw) editMode players (Match a b res) ppoints1 ppoints2 = el "div" $ do
  rec
    let
      (ppoints1', ppoints2') =
        fmap fst &&& fmap snd $ liftA3 (currentPoints stw) ppoints1 ppoints2 matchResult
    bLeft <- divClass "column1" $
      playerButton editMode ((!!a) <$> players) ppoints1' LeftPlayer matchResult
    matchResult' <- zipDynWithChangeOnFst (\ _ x -> x)
      (zipDyn editMode $ fmap winner <$> matchResult)
      matchResult
    setChangeE <- divClass "column2" $
      editableComponentWithEvent editMode
        (dyn_ $ maybe (text "-") (text . printMatchResult " : " stw) <$> matchResult)
        (componentOnDyn (setResult stw) matchResult')
    matchResult <- accumDyn collectMatchEvents res $ leftmost [
      WinnerToggle <$> bLeft,
      WinnerToggle <$> bRight,
      SetChange <$> setChangeE]
    bRight <- divClass "column3" $
      playerButton editMode ((!!b) <$> players) ppoints2' RightPlayer matchResult
  return (Match a b <$> matchResult, (ppoints1', ppoints2'))

currentPoints :: Natural -> Int -> Int -> Maybe MatchResult -> (Int, Int)
currentPoints stw p1 p2 = maybe (p1, p2) (pointsAfterMatch p1 p2 stw)

playerButton :: (DomBuilder t m, PostBuild t m, MonadHold t m) =>
   Dynamic t Bool ->
   Dynamic t Participant ->
   Dynamic t Int ->
   Player ->
   Dynamic t (Maybe MatchResult) -> m (Event t Player)
playerButton editMode player ppoints pos matchResult = editableComponentWithEvent editMode
  (dynText $ name <$> player)
  (playerButton' player ppoints pos matchResult)

playerButton' :: (PostBuild t m, DomBuilder t m) =>
  Dynamic t Participant ->
  Dynamic t Int ->
  Player ->
  Dynamic t (Maybe MatchResult) -> m (Event t Player)
playerButton' player ppoints pos matchResult = do
  (e, _) <- elDynAttr' "button" (pickColor pos <$> matchResult) .
    dynText $ zipDynWith showWithPoints player ppoints
  return $ pos <$ domEvent Click e

showWithPoints :: Participant -> Int -> Text
showWithPoints p pts = T.concat [name p, " (", T.pack $ show pts, ")"]

setResult :: DomBuilder t m => Natural -> Maybe MatchResult -> m (Event t MatchResult)
setResult stw (Just (MatchResult LeftPlayer osets)) = do
  text $ T.pack (show stw) <> " : "
  os <- setInput stw osets
  pure $ MatchResult LeftPlayer <$> os
setResult stw (Just (MatchResult RightPlayer osets)) = do
  os <- setInput stw osets
  text $ " : " <> T.pack (show stw)
  pure $ MatchResult RightPlayer <$> os
setResult _ Nothing = pure never

setInput :: DomBuilder t m => Natural -> OpponentSets -> m (Event t OpponentSets)
setInput stw osets = do
  n <- inputElement $ def
    & inputElementConfig_initialValue .~ (T.pack . show) osets
    & inputElementConfig_elementConfig . elementConfig_initialAttributes .~
      ("type" =: "number" <> "min" =: "0" <> "max" =: T.pack (show $ stw - 1))
  return . fmap (read . T.unpack) $ _inputElement_input n

collectMatchEvents :: Maybe MatchResult -> MatchStateEvent -> Maybe MatchResult
collectMatchEvents (Just (MatchResult w _)) (WinnerToggle wt) | w == wt = Nothing
collectMatchEvents _ (WinnerToggle wt) = Just $ MatchResult wt 0
collectMatchEvents _ (SetChange r) = Just r

pickColor :: Player -> Maybe MatchResult -> Map Text Text
pickColor player mRes =
  if Just player == (winner <$> mRes)
  then "style" =: "color: red"
  else Map.empty
