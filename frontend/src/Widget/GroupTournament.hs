{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE FlexibleContexts #-}

module Widget.GroupTournament where

import Control.Monad
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as T
import Numeric.Natural (Natural)

import Reflex.Dom.Core hiding (Group)

import CommonTypes
import Common.Message (C2S(..), Lettering(..), S2C(..))
import Group.CompleteCompetition
import Group.GroupTypes
import Parser.GroupTournamentParser
import Widget.MessageChannel (sendAndReceiveMessages)

tournamentFile :: Text -> FilePath
tournamentFile a = T.unpack $ T.concat ["group_", a ,".txt"]

matchResultOptions :: Natural -> Map (Maybe MatchResult) Text
matchResultOptions n = Map.fromList $
  (Nothing, "-") :
  zip (Just <$> possibleMatchResults n) (printMatchResult " : " n <$> possibleMatchResults n)

initialGroups :: [Group]
initialGroups = [Group
  ["Spieler 1", "Spieler 2", "Spieler 3", "Spieler 4"]
  (CompleteCompetition [[Nothing, Nothing, Nothing], [Nothing, Nothing], [Nothing]])]

groupTournament :: (
  DomBuilder t m,
  PostBuild t m,
  MonadHold t m,
  Prerender t m,
  PerformEvent t m,
  MonadIO (Performable m),
  MonadFix m) => Maybe Text -> Text -> m ()
groupTournament baseRoute tName = do
  rec
    let
      tFile = tournamentFile tName
      initParams = TournamentParameters 3
    params <- holdDyn initParams (tournamentParameters <$> load)
    save <- tag (current $ zipDynWith GroupTournament params tournamentData) <$> button "Speichern"
    _ <- sendAndReceiveMessages baseRoute $ C2SWrite tFile . showTournament <$> save
    msgLoad <- button "Laden" >>= sendAndReceiveMessages baseRoute . (C2SLoad tFile <$)
    let (loadError, load) = fanEither $ parseTournament . fileContent <$> msgLoad
    performEvent_ $ liftIO . print <$> loadError
    tournamentData <- join <$>
      widgetHold
        (tournamentWidget baseRoute tName $ GroupTournament initParams initialGroups)
        (tournamentWidget baseRoute tName <$> load)
  return ()

tournamentWidget :: (
  DomBuilder t m,
  PostBuild t m,
  MonadHold t m,
  Prerender t m,
  MonadFix m) => Maybe Text -> Text -> GroupTournament -> m (Dynamic t [Group])
tournamentWidget baseRoute tName (GroupTournament params@(TournamentParameters stw) groups) = do
  createSheets <- button "Match-Zettel erstellen"
  let
    message = C2SMatchSheets
      (T.unpack tName)
      stw
      (zip [1..] groups >>= uncurry (createLettering tName))
  _ <- sendAndReceiveMessages baseRoute $ message <$ createSheets
  divClass "group-grid" .
    fmap sequenceA .
    traverse (uncurry $ groupBoard params) $
    zip [0..] groups

createLettering :: Text -> Int -> Group -> [Lettering]
createLettering tName n (Group playerNames _) =
  uncurry (Lettering $ T.concat [groupLabel n, " (", tName, ")"]) <$> matchPairs playerNames

matchPairs :: [Text] -> [(Text, Text)]
matchPairs [] = []
matchPairs (a:as) = fmap ((,) a) as <> matchPairs as

groupLabel :: Int -> Text
groupLabel n = "Gruppe " <> T.pack (show n)

groupBoard :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) =>
  TournamentParameters -> Int -> Group -> m (Dynamic t Group)
groupBoard (TournamentParameters stw) groupNr (Group playerNames (CompleteCompetition mss)) =
  divClass "group" $ do
    rec
      el "h3" . text . groupLabel $ groupNr+1
      elClass "table" "ranking" $ do
        el "thead" . el "tr" $ do
          el "td" blank
          mapM_ (elClass "td" "balance-col") [text "Matches", text "Sätze"]
        el "tbody" . dyn_ $
          mapM_ playerBalanceRow . competitionRanking stw playerNames <$> matchResults
      matchResults <- elClass "table" "matches" . el "tbody" $ do
        el "tr" $ el "td" blank >> mapM_ (elClass "td" "match-col" . text) (drop 1 playerNames)
        fmap (fmap CompleteCompetition . traverse sequenceA) .
          forM (zip playerNames mss) $ \(playerName, ms) ->
            el "tr" $ do
              elClass "td" "player" $ text playerName
              replicateM_ (length playerNames - 1 - length ms) . el "td" $ blank
              forM ms $ \m ->
                elClass "td" "centered-td" $
                  _dropdown_value <$> dropdown m (constDyn $ matchResultOptions stw) def
    pure $ Group playerNames <$> matchResults

playerBalanceRow :: DomBuilder t m => PlayerBalance Text -> m ()
playerBalanceRow (PlayerBalance n s s') = el "tr" $ do
  elClass "td" "player" $ text n
  mapM_ (elClass "td" "centered-td" . text) [g (ms s) (ms <$> s'), g (sets s) (sets <$> s')]
    where
      f (a,b) = T.intercalate " : " $ T.pack . show <$> [a,b]
      g a b = foldl (\ x y -> T.concat [x, " (", y, ")"]) (f a) (f <$> b)
      ms (Stats w l _ _) = (w,l)
      sets (Stats _ _ sw sl) = (sw,sl)
