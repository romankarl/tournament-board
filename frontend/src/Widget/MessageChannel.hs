{-# LANGUAGE OverloadedStrings #-}

module Widget.MessageChannel where

import qualified Data.Aeson as Aeson
import Data.ByteString.Lazy (fromStrict, toStrict)
import Data.List.NonEmpty (nonEmpty)
import Data.Text (Text)
import Obelisk.Route
import Text.URI

import Reflex.Dom.Core

import Common.Message (C2S(..), S2C(..))
import Common.Route

sendAndReceiveMessages :: (Monad m, Prerender t m) => Maybe Text -> Event t C2S -> m (Event t S2C)
sendAndReceiveMessages r msg = do
  wsRespEv <- fmap switchDyn $ prerender (return never) $ do
    case checkEncoder fullRouteEncoder of
      Left err -> do
        el "div" $ text err
        return never
      Right encoder -> do
        let
          wsPath = fst $ encode encoder $ FullRoute_Backend BackendRoute_WebSocket :/ ()
          sendEv = (:[]) . toStrict . Aeson.encode <$> msg
        let mUri = do
              uri' <- mkURI =<< r
              pathPiece <- nonEmpty =<< mapM mkPathPiece wsPath
              wsScheme <- case uriScheme uri' of
                rtextScheme | rtextScheme == mkScheme "https" -> mkScheme "wss"
                rtextScheme | rtextScheme == mkScheme "http" -> mkScheme "ws"
                _ -> Nothing
              return $ uri'
                { uriPath = Just (False, pathPiece)
                , uriScheme = Just wsScheme
                }
        case mUri of
          Nothing -> return never
          Just uri -> do
            ws <- webSocket (render uri) $ def & webSocketConfig_send .~ sendEv
            return (_webSocket_recv ws)
  pure $ fmapMaybe (Aeson.decode . fromStrict) wsRespEv
