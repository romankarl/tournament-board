{-# LANGUAGE OverloadedStrings #-}

module CommonTypes where

import Control.Applicative (liftA2)
import Data.Text (Text)
import qualified Data.Text as T
import Numeric.Natural (Natural)

newtype TournamentParameters = TournamentParameters {setsToWin :: Natural}

showTournamentParameters :: TournamentParameters -> Text
showTournamentParameters (TournamentParameters stw) = "setsToWin " <> T.pack (show stw)

data Player = LeftPlayer | RightPlayer deriving (Eq, Ord, Show)
type OpponentSets = Natural
data MatchResult = MatchResult {winner :: Player, opponentSets :: Natural} deriving (Eq, Ord, Show)

showMatchResult :: MatchResult -> Text
showMatchResult (MatchResult LeftPlayer osets) = ":" <> T.pack (show osets)
showMatchResult (MatchResult RightPlayer osets) = T.pack (show osets) <> ":"

possibleMatchResults :: Natural -> [MatchResult]
possibleMatchResults n = liftA2 MatchResult [LeftPlayer, RightPlayer] [0 .. n-1]

printMatchResult :: Text -> Natural -> MatchResult -> Text
printMatchResult sep wsets (MatchResult LeftPlayer osets) =
  T.intercalate sep $ T.pack . show <$> [wsets, osets]
printMatchResult sep wsets (MatchResult RightPlayer osets) =
  T.intercalate sep $ T.pack . show <$> [osets, wsets]
