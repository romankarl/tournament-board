module Swiss.Points (
  Player(..), OpponentSets, MatchResult(..), pointsAfterMatch) where

import Control.Arrow ((&&&))
import Data.Tuple (swap)
import Numeric.Natural (Natural)

import CommonTypes

pointsAfterMatch :: Int -> Int -> Natural -> MatchResult -> (Int, Int)
pointsAfterMatch p1 p2 wsets (MatchResult LeftPlayer osets) =
  pointsAfterMatch' p1 p2 wsets osets
pointsAfterMatch p1 p2 wsets (MatchResult RightPlayer osets) =
  swap $ pointsAfterMatch' p2 p1 wsets osets

pointsAfterMatch' :: Int -> Int -> Natural -> OpponentSets -> (Int, Int)
pointsAfterMatch' pwinner plooser wsets osets =
  (+) pwinner &&& (-) plooser $ exchangePoints (setsValue wsets osets) (pwinner - plooser)

setsValue :: Natural -> OpponentSets -> Int
setsValue 2 osets = 33 - 6 * (fromInteger . toInteger) osets
setsValue 3 osets = 35 - 5 * (fromInteger . toInteger) osets

exchangePoints :: Int -> Int -> Int
exchangePoints d diff
  | diff >= 0 = round (fromIntegral d * exp (-(sq x / (2 * sq variance))) :: Double)
  | otherwise = d + round (fromIntegral d * c2 * (1 - exp (x/c1)) * sqrt (-x) :: Double)
  where
    x = fromIntegral diff
    variance = 120
    c1 = 100
    c2 = 1 / 35
    sq a = a ^ (2::Int)
