{-# LANGUAGE OverloadedStrings #-}

module Swiss.SwissTypes where

import Data.Maybe (maybeToList)
import Data.Text (Text)
import qualified Data.Text as T

import CommonTypes

data SwissSystemTournament = SwissSystemTournament {
  tournamentParameters :: TournamentParameters,
  tournamentParticipants :: [Participant],
  tournamentRounds :: [Round]
}

data Participant = Participant {
  name :: Text,
  points :: Int,
  inactive :: Bool,
  entryRound :: Int} deriving (Eq, Ord, Show)
type ParticipantId = Int

showParticipant :: Participant -> Text
showParticipant (Participant n _ i e) = T.unwords [T.pack $ show e, if i then "i" else "a", n]

data Match = Match {
  player1 :: ParticipantId,
  player2 :: ParticipantId,
  result :: Maybe MatchResult} deriving (Eq, Show)

showMatch :: Match -> Text
showMatch (Match p1 p2 r) = T.unwords $ T.pack (show p1) : T.pack (show p2) : maybeToList (showMatchResult <$> r)

data Round = Round {
  matches :: [Match],
  bye :: Maybe ParticipantId,
  inactivePlayers :: [ParticipantId]} deriving (Eq, Show)

showRound :: Round -> Text
showRound (Round ms b is) = T.concat . fmap (<> "\n") $
  T.intercalate ", " (showMatch <$> ms) :
  maybeToList ((byeLabel <>) . T.pack . show <$> b) ++
  [inactiveLabel <> T.unwords (T.pack . show <$> is) | not (null is)]

byeLabel, inactiveLabel :: Text
byeLabel = "bye: "
inactiveLabel = "inactive: "

showTournament :: SwissSystemTournament -> Text
showTournament (SwissSystemTournament params ps rs) = T.intercalate "\n\n" [
  showTournamentParameters params,
  T.intercalate "\n" $ showParticipant <$> ps,
  T.intercalate "\n" $ showRound <$> rs]

showNames :: [Participant] -> Text
showNames = T.concat . fmap ((<> "\n") . name)
