module Swiss.Pairing (
  Pairing(Against, Bye), Solution(Solution), UnorderedPair(UnorderedPair), solution, main) where

import Control.Applicative (liftA2)
import Control.Arrow ((&&&))
import Data.Foldable (foldl')
import Data.Functor.Base (TreeF (NodeF))
import Data.Functor.Foldable (refold)
import Data.List (elemIndex, sort, unfoldr)
import Data.Maybe (catMaybes, listToMaybe, mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set

data UnorderedPair a = UnorderedPair a a deriving Show
data Pairing a = Against a a | Bye a deriving Show
type Ranking a = [a]
data Solution a = Solution [UnorderedPair a] (Maybe a) deriving Show
data Node = Node {
  remainingParticipants :: Ranking Int,
  scheduledPairings :: [(Int, Int)]} deriving Show

instance Eq a => Eq (Pairing a) where
  Against a b == Against c d = a == c && b == d || a == d && b == c
  Bye a == Bye b = a == b
  _ == _ = False

instance Ord a => Ord (Pairing a) where
  compare (Against a b) (Against c d) = compare (sort [a,b]) (sort [c,d])
  compare (Bye a) (Bye b) = compare a b
  compare (Bye _) (Against _ _) = LT
  compare (Against _ _) (Bye _) = GT

main :: IO ()
main = mapM_ print . take 12 $ iterateSolution 28

iterateSolution :: Int -> [Solution Int]
iterateSolution n = unfoldr coalg Set.empty
  where
    coalg constraints =
      (id &&& Set.union constraints . Set.fromList . toConstraints) <$> solution constraints [1..n]
    toConstraints (Solution as b) = foldl (flip (:)) (toAgainst <$> as) (Bye <$> b)
    toAgainst (UnorderedPair a b) = Against a b

solution :: Eq a => Set (Pairing a) -> Ranking a -> Maybe (Solution a)
solution constraints as = toSolution as <$> solution' cs [0 .. roundDownToOdd (length as)]
  where
    cs = Set.fromList . mapMaybe (fromPairing as) $ Set.toList constraints
    roundDownToOdd n = n - 1 + n `mod` 2

fromPairing :: Eq a => [a] -> Pairing a -> Maybe (Int,Int)
fromPairing as (Against b c) =
  liftA2 (\ x y -> if x < y then (x,y) else (y,x)) (elemIndex b as) (elemIndex c as)
fromPairing as _ | even (length as) = Nothing
fromPairing as (Bye b) = flip (,) (length as) <$> elemIndex b as

toSolution :: [a] -> [(Int, Int)] -> Solution a
toSolution as = foldl' (addToSolution as) (Solution [] Nothing)

addToSolution :: [a] -> Solution a -> (Int, Int) -> Solution a
addToSolution as (Solution ps _) (b, c) | c == length as = Solution ps $ Just (as!!b)
addToSolution as (Solution ps bye) (b, c) = Solution (UnorderedPair (as!!b) (as!!c) : ps) bye

solution' :: Set (Int, Int) -> Ranking Int -> Maybe [(Int, Int)]
solution' constraints = refold combine branch . flip Node []
  where
    branch :: Node -> TreeF Node Node
    branch node@(Node [] _) = NodeF node []
    branch node@(Node (p1:ps) pairings) = NodeF node . mapMaybe (createNode p1 pairings) $ picks ps

    createNode :: Int -> [(Int, Int)] -> (Int, Ranking Int) -> Maybe Node
    createNode p1 pairings (p2, r)
      | Set.member (p1,p2) constraints = Nothing
      | bottomClusterSize constraints r * 2 > length r = Nothing
      | otherwise = Just . Node r $ (p1,p2) : pairings

combine :: TreeF Node (Maybe [(Int, Int)]) -> Maybe [(Int, Int)]
combine (NodeF (Node ps pairings) []) = if null ps then Just pairings else Nothing
combine (NodeF _ ss) = listToMaybe $ catMaybes ss

bottomClusterSize :: Set (Int, Int) -> Ranking Int -> Int
bottomClusterSize constraints ps = length $ bottomCluster (reverse ps) []
  where
    bottomCluster :: [Int] -> [Int] -> [Int]
    bottomCluster (a:as) bs | all (\ x -> Set.member (a,x) constraints) bs = bottomCluster as (a:bs)
    bottomCluster _ bs = bs

picks :: [a] -> [(a, [a])]
picks [] = []
picks (a:as) = (a, as) : fmap (fmap (a:)) (picks as)
