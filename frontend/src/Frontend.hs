{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}

module Frontend where

import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import qualified Obelisk.Configs as Cfg
import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Route.Frontend
import Obelisk.Generated.Static
import Reflex.Dom.Core

import Common.Route
import Widget.GroupTournament
import Widget.SwissSystemTournament (swissSystemTournament)

-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = subRoute_ $ \case
      FrontendRoute_SwissSystemTournament -> do
        tName <- askRoute
        el "title" . dynText $ tName <> " (Schweizer System)"
        cssEl $(static "swiss.css")
      FrontendRoute_GroupTournament -> do
        tName <- askRoute
        el "title" . dynText $ tName <> " (Gruppen)"
        cssEl $(static "group.css")
  , _frontend_body = do
      configs <- Cfg.getConfigs
      let r = T.strip . decodeUtf8 <$> Map.lookup "common/route" configs
      subRoute_ $ \case
        FrontendRoute_SwissSystemTournament -> askRoute >>= dyn_ . fmap (swissSystemTournament r)
        FrontendRoute_GroupTournament -> askRoute >>= dyn_ . fmap (groupTournament r)
  }

cssEl :: DomBuilder t m => Text -> m ()
cssEl cssFile = elAttr
  "link"
  ("href" =: cssFile <> "type" =: "text/css" <> "rel" =: "stylesheet")
  blank
