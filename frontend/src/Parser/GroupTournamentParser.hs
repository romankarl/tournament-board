module Parser.GroupTournamentParser (parseTournament) where

import Data.Text (Text)
import Text.Parsec.Error (ParseError)
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Combinator

import CommonTypes
import Group.CompleteCompetition
import Group.GroupTypes (Group(Group), GroupTournament(GroupTournament))
import Parser.CommonParser

parseTournament :: String -> Either ParseError GroupTournament
parseTournament = parse tournamentP ""

tournamentP :: Parsec String u GroupTournament
tournamentP = GroupTournament <$> paramP <* many1 endOfLine <*> groupsP

groupsP :: Parsec String u [Group]
groupsP = groupP `sepEndBy` many1 endOfLine

groupP :: Parsec String u Group
groupP = formGroup <$> playersP <* endOfLine <*> optionMaybe resultsP

playersP :: Parsec String u [Text]
playersP = nameP' `sepEndBy1` endOfLine

resultsP :: Parsec String u CompleteCompetition
resultsP = CompleteCompetition <$> resultOptionP `sepBy1` char ' ' `sepEndBy1` endOfLine

resultOptionP :: Parsec String u (Maybe MatchResult)
resultOptionP = (Nothing <$ char '-') <|> (Just <$> resultP)

formGroup :: [Text] -> Maybe CompleteCompetition -> Group
formGroup ns Nothing = Group ns . emptyCompetition $ length ns
formGroup ns (Just c) = Group ns c
