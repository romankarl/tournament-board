module Parser.SwissSystemTournamentParser (parseTournament) where

import Text.Parsec.Error (ParseError)
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Combinator
import qualified Data.Text as T

import Parser.CommonParser
import Swiss.SwissTypes

parseTournament :: String -> Either ParseError SwissSystemTournament
parseTournament = parse tournamentP ""

tournamentP :: Parsec String u SwissSystemTournament
tournamentP = SwissSystemTournament <$>
  paramP <* many1 endOfLine <*> participantsP <* many1 endOfLine <*> roundsP

participantsP :: Parsec String u [Participant]
participantsP = participantP `sepEndBy` endOfLine

participantP :: Parsec String u Participant
participantP = (\e i n -> Participant n 1000 i e) <$>
  intP <* char ' ' <*> inactiveP <* char ' ' <*> nameP

inactiveP :: Parsec String u Bool
inactiveP = (False <$ char 'a') <|> (True <$ char 'i')

roundsP :: Parsec String u [Round]
roundsP = roundP `sepEndBy` many1 endOfLine

roundP :: Parsec String u Round
roundP = Round <$> matchP `sepBy1` string ", " <*>
  optionMaybe (try $ endOfLine *> string (T.unpack byeLabel) *> intP) <*>
  option [] (try $ endOfLine *> string (T.unpack inactiveLabel) *> intP `sepBy1` char ' ')

matchP :: Parsec String u Match
matchP = Match <$> intP <* char ' ' <*> intP <*> optionMaybe (char ' ' *> resultP)
