module Parser.CommonParser where

import Control.Applicative (some)
import Data.Text (Text)
import qualified Data.Text as T
import Numeric.Natural (Natural)
import Text.Parsec.Prim
import Text.Parsec.Char

import CommonTypes

paramP :: Parsec String u TournamentParameters
paramP = TournamentParameters <$ string "setsToWin " <*> natP

nameP :: Parsec String u Text
nameP = T.pack <$> many (noneOf "\r\n")

nameP' :: Parsec String u Text
nameP' = T.pack <$> some (noneOf "\r\n")

resultP :: Parsec String u MatchResult
resultP = MatchResult LeftPlayer <$ char ':' <*> natP <|>
  MatchResult RightPlayer <$> natP <* char ':'

intP :: Parsec String u Int
intP = read <$> many digit

natP :: Parsec String u Natural
natP = read <$> many digit
