# Tournament Board

A tournament board for group tournaments and swiss system tournaments targeted on table tennis tournaments.

## Prerequisites

- [Nix](https://nixos.org/download/)
- [Obelisk](https://github.com/obsidiansystems/obelisk)

## Set up

### Development mode

`ob run` will start the web server in development mode.

### Regular setup

1. compile the code with Nix

       nix-build -A exe --no-out-link

2. Link or copy data accordingly. An example setup can be found in the [Obelisk documentation](https://github.com/obsidiansystems/obelisk?tab=readme-ov-file#locally).

3. Afterwards the executable can be run.

       LANG=C.UTF-8 ./backend

## Run

Choose tournament mode and name and open the site in a browser:

For swiss system tournaments:
http://localhost:8000/swiss/TournamentName

For group tournaments:
http://localhost:8000/group/TournamentName

## Limitations

Group tournaments other than the demo tournament can only be created from previously created files.
