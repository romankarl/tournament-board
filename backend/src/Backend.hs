{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Backend where

import Control.Monad (forever)
import Data.Aeson (decode, encode)
import qualified Data.ByteString as B
import Data.ByteString.Lazy (toStrict)
import Data.Dependent.Sum (DSum (..))
import Data.Functor.Identity
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Obelisk.Backend
import qualified Network.WebSockets as WS
import Network.WebSockets.Snap (runWebSocketsSnap)

import Common.Message
import Common.Route
import MatchSheets (printMatchSheets)

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
      serve $ \case
        BackendRoute_Missing :=> Identity () -> return ()
        BackendRoute_WebSocket :=> Identity () -> do
          runWebSocketsSnap application
  , _backend_routeEncoder = fullRouteEncoder
  }

application :: WS.ServerApp
application pending = do
  conn <- WS.acceptRequest pending
  WS.withPingThread conn 30 (pure ()) . forever $ do
    msgbs <- WS.receiveData conn :: IO B.ByteString
    let msgC = decode $ WS.toLazyByteString msgbs :: Maybe C2S
    case msgC of
      Nothing ->
        T.putStrLn "decoded message is nothing..."
      Just (C2SWrite tFile content) -> do
        putStrLn $ "write file " <> tFile
        writeFile tFile $ T.unpack content
      Just (C2SLoad tFile) -> do
        putStrLn $ "read file " <> tFile
        readFile tFile >>= WS.sendTextData conn . toStrict . encode . S2CLoad
      Just (C2SMatchSheets nameForFile setsToWin lettering) -> do
        putStrLn $ "print match sheets for " <> nameForFile
        printMatchSheets nameForFile setsToWin lettering
