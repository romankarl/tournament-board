{-# LANGUAGE OverloadedStrings #-}

module MatchSheets (Lettering(..), printMatchSheets, main) where

import Data.Foldable (for_)
import Data.List (unfoldr)
import Data.Text (Text)
import qualified Data.Text as Text
import Graphics.PDF
import Numeric.Natural (Natural)

import Common.Message (Lettering(..))

data Layout = Layout3 | Layout5

pickLayout :: Natural -> Layout
pickLayout 2 = Layout3
pickLayout _ = Layout5

testPlayers :: [(Text, Text)]
testPlayers = unfoldr coalg $ (\n -> "Spieler " <> Text.pack (show n)) <$> ([1..20] :: [Int])
  where
    coalg (a:b:as) = Just ((a,b), as)
    coalg _ = Nothing

testInfos :: [Text]
testInfos = ["A", "B", "C", "D"] >>= (\ g -> replicate 3 ("Gruppe " <> g <> " (Niedrig)"))

pWidth, pHeight, width, height :: Double
pWidth = 29.7/2.54*72.0
pHeight = 21.0/2.54*72.0
width = pWidth/2
height = pHeight/3

main :: IO ()
main = printMatchSheets "match_sheets" 3 $ zipWith (uncurry . Lettering) testInfos testPlayers

printMatchSheets :: String -> Natural -> [Lettering] -> IO ()
printMatchSheets nameForFile setsToWin lettering = do
  Right timesRoman <- mkStdFont Times_Roman
  runPdf (nameForFile <> ".pdf") standardDocInfo (PDFRect 0 0 pWidth pHeight) $
    matchSheetDocument (pickLayout setsToWin) timesRoman lettering

matchSheetDocument :: Layout -> AnyFont -> [Lettering] -> PDF ()
matchSheetDocument layout timesRoman lettering = mapM_ (createPage layout timesRoman) $
  unfoldr (\ps -> if null ps then Nothing else Just $ splitAt 6 ps) lettering

createPage :: Layout -> AnyFont -> [Lettering] -> PDF ()
createPage layout timesRoman lettering = do
  page <- addPage Nothing
  drawWithPage page $ do
    let
      border = 1/2.54*72.0
      line x1 y1 x2 y2 = moveto (x1 :+ y1) >> lineto (x2 :+ y2)
      vline x = line x border x (pHeight - border)
      hline y = line border y (pWidth - border) y
      drawColumn w l = mapM_ (uncurry $ drawMatchSheet layout timesRoman w) $
        zip [2*height, height, 0] l
    vline width
    hline height
    hline (2*height)
    strokePath
    drawColumn 0 $ take 3 lettering
    drawColumn width $ drop 3 lettering
  
drawMatchSheet :: Layout -> AnyFont -> Double -> Double -> Lettering -> Draw ()
drawMatchSheet layout timesRoman x y (Lettering info leftPlayer rightPlayer) = do
  let
    drawText' t xd yd = drawText $ text (PDFFont timesRoman 11) (x+xd) (y+yd) t
    xos = [width/4, width/2, width*3/4]
    lay Layout3 = flip (,) 100 <$> xos
    lay Layout5 = (flip (,) 115 <$> xos) ++ (flip (,) 75 <$> take 2 xos)
  drawText' leftPlayer 40 (height - 45)
  drawText' rightPlayer 235 (height - 45)
  drawText' info 290 40
  for_ (lay layout) . uncurry $ drawText' ":"
